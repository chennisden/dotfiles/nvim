-- options
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.autoindent = true

vim.opt.wildmode = {'longest', 'list', 'full'}

-- Language to use when spellcheck is enabled
vim.opt.spelllang = "en_us"

-- Highlight (undesirable) trailing whitespace
vim.cmd 'hi TrailingWhitespace ctermbg=red guibg=red'
vim.w.m1 = vim.fn.matchadd("TrailingWhitespace", '\\v\\s+$')
--- Remove trailing whitespace
vim.api.nvim_set_keymap("n", "<F5>", "[[:let _s=@/<Bar>:%s/\\s\\+$//e<Bar>:let @/=_s<Bar><CR>]]", {})
