vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
vim.api.nvim_set_keymap("n", "<F4>", [[:%s/\.\$/$./ge\|:%s/\,\$/$,/ge<CR>]], {})
