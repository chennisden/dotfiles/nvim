vim.opt.spell = true
vim.api.nvim_create_user_command("W", "w | !typst compile %&", {})
vim.api.nvim_set_keymap("n", "<F4>", [[:%s/\.\$/$./g|:%s/\,\$/$,/g]], {})
