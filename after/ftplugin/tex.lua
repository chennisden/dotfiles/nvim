vim.opt.spell = true
vim.g.tex_flavor = "latex"
vim.api.nvim_create_user_command("W", "w | silent !dlatexmk %", {})
vim.api.nvim_set_keymap("n", "<F4>", [[:%s/\.\$/$./g|:%s/\,\$/$,/g]], {})
