vim.opt.spell = true
vim.api.nvim_create_user_command("W", "w | silent !rust-beam --fragile % &", {})
vim.api.nvim_set_keymap("n", "<F4>", [[:%s/\.\$/$./ge\|:%s/\,\$/$,/ge<CR>]], {})
